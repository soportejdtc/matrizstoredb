package com.jdtc.matrizstoredb;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by johan on 5/03/17.
 */
public class MatrizTest {
    private Matriz matriz;

/*
    @Test
    public void toArray() throws Exception {

    }
*/

    @Before
    public void setUp() throws Exception {
        matriz = new Matriz(2, 2);
        matriz.setElementAt(1, 1, 1);
        matriz.setElementAt(1, 2, 2);
        matriz.setElementAt(2, 1, 3);
        matriz.setElementAt(2, 2, 4);
    }

    @Test
    public void toArray() throws Exception {
        int number = 5;
        int row = 2;
        int col = 1;

        System.out.println(" -- Before toArray() -- ");
        System.out.println("matriz = " + matriz);

        Number[][] matrizArray = matriz.toArray();
        matrizArray[row - 1][col - 1] = 5;

        System.out.println(" -- After toArray() -- ");
        System.out.println("matrizArray = " + Arrays.deepToString(matrizArray));
        System.out.println("matriz = " + matriz);

        assertNotEquals(number, matriz.getElementAt(row, col));
    }

    @Test
    public void toStringTest() throws Exception {
        System.out.println("matriz = " + matriz);
        assertTrue(true);
    }

}