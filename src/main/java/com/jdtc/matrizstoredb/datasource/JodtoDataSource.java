package com.jdtc.matrizstoredb.datasource;

import org.postgresql.ds.PGPoolingDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JodtoDataSource {

    private static PGPoolingDataSource dataSource;

    public static DataSource getDataSource() {
        if (dataSource == null) {
            dataSource = new PGPoolingDataSource();
            dataSource.setDatabaseName("JodtoDS");
            dataSource.setServerName("localhost");
            dataSource.setDatabaseName("jodtoc");
            dataSource.setCurrentSchema("store_matriz_db");
            dataSource.setUser("jdtc");
            dataSource.setPassword("jdtc");
            dataSource.setMaxConnections(10);
        }

        return dataSource;
    }

    public static Connection getConnection(){
        Connection connection = null;
        if(dataSource==null){
            getDataSource();
        }

        try {
            connection =  dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }
}
