package com.jdtc.matrizstoredb;

import com.jdtc.matrizstoredb.datasource.JodtoDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MatrizDAO {

    private final String SEQUENCE_NAME = "matriz_sequence";
    private DataSource dataSource;

    public MatrizDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static void main(String[] args) {
        Matriz matriz = new Matriz(2, 2);
        matriz.setElementAt(1, 1, 1);
        matriz.setElementAt(1, 2, 2);
        matriz.setElementAt(2, 1, 3);
        matriz.setElementAt(2, 2, 4);

        //new MatrizDAO(JodtoDataSource.getDataSource()).save(matriz);
        Matriz m = new MatrizDAO(JodtoDataSource.getDataSource()).getById(1);
        System.out.println("m = " + m);
    }

    public void save(Matriz matriz) {
        String sql = "INSERT INTO matriz(matriz, row, col, value) VALUES (?, ?, ?, ?)";

        try (Connection connection = getConnection();
             PreparedStatement pstmt = connection.prepareStatement(sql)) {

            int id = getSequenceNextVal(connection, SEQUENCE_NAME);

            Number[][] values = matriz.toArray();

            for (int row = 0; row < matriz.getRows(); row++) {
                for (int col = 0; col < matriz.getColumns(); col++) {
                    int i = 1;

                    pstmt.setInt(i++, id);
                    pstmt.setInt(i++, row);
                    pstmt.setInt(i++, col);
                    pstmt.setObject(i, values[row][col]);

                    pstmt.addBatch();
                    pstmt.clearParameters();
                }
            }

            pstmt.executeBatch();
            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    

    public Matriz getById(int matrizID) {
        String sql;
        int rows = 0;
        int columns = 0;

        sql = "SELECT MAX(row) as rows , MAX(col) as columns FROM matriz WHERE matriz = ?";

        try (Connection connection = getConnection();
             PreparedStatement pstmt = connection.prepareStatement(sql)) {

            pstmt.setInt(1, matrizID);
            ResultSet resultSet = pstmt.executeQuery();

            if (resultSet.next()) {
                rows = resultSet.getInt(1);
                columns = resultSet.getInt(2);
            }

            //pstmt.executeQuery()
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Matriz matriz = new Matriz(rows + 1, columns +1);

        sql = "SELECT row, col, value FROM matriz WHERE matriz = ? ";

        try (Connection connection = getConnection();
             PreparedStatement pstmt = connection.prepareStatement(sql)) {

            pstmt.setInt(1, matrizID);
            ResultSet resultSet = pstmt.executeQuery();

            int row, col;
            Number value;
            while (resultSet.next()) {
                row = resultSet.getInt(1) ;
                col = resultSet.getInt(2) ;
                value = (Number) resultSet.getObject(3);
                matriz.setElementAt(row + 1, col+1, value);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return matriz;
    }

    private Connection getConnection() {
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    private int getSequenceNextVal(Connection connection, String sequence) {
        String sql = "SELECT NEXTVAL(?)";
        int nextVal = -1;

        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {

            pstmt.setString(1, sequence);

            ResultSet resultset = pstmt.executeQuery();
            if (resultset.next()) {
                nextVal = resultset.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return nextVal;
    }

}
