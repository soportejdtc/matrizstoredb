package com.jdtc.matrizstoredb;

import java.util.Arrays;

public class Matriz {
    private int id;
    private Number[][] matriz;
    private int rows;
    private int columns;

    public Matriz(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.matriz = new Number[rows][columns];
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Number getElementAt(int row, int column) {
        return matriz[--row][--column];
    }

    public void setElementAt(int row, int column, Number number) {
        matriz[--row][--column] = number;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public Number[][] toArray() {
        Number[][] copyMatriz = new Number[rows][columns];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                copyMatriz[row][col] = matriz[row][col];
            }
        }

        return copyMatriz;
    }

    @Override
    public String toString() {
        return Arrays.deepToString(matriz);
    }
}
